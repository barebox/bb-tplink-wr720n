/* Register Descriptions */
/* CPU Mapped Registers Summary */

/* 6.1 DDR Registers */
#define DDR_CONFIG 0x18000000 /* DDR DRAM Configuration */
#define DDR_CONFIG2 0x18000004 /* DDR DRAM Configuration */
#define DDR_MODE_REGISTER 0x18000008 /* DDR Mode Value */
#define DDR_EXTENDED_MODE_REGISTER 0x1800000C /* DDR Extended Mode Value */
#define DDR_CONTROL 0x18000010 /* DDR Control */
#define DDR_REFRESH 0x18000014 /* DDR Refresh Control and Configuration */
#define DDR_RD_DATA_THIS_CYCLE 0x18000018 /* DDR Read Data Capture Bit Mask */
#define TAP_CONTROL_0 0x1800001C /* DQS Delay Tap Control for Byte 0 */
#define TAP_CONTROL_1 0x18000020 /* DQS Delay Tap Control for Byte 1 */
#define DDR_WB_FLUSH_GE0 0x1800007C /* GE0 Interface Write Buffer Flush */
#define DDR_WB_FLUSH_GE1 0x18000080 /* GE1 Interface Write Buffer Flush */
#define DDR_WB_FLUSH_USB 0x18000084 /* USB Interface Write Buffer Flush */
#define DDR_DDR2_CONFIG 0x1800008C /* DDR2 Configuration */
#define DDR_EMR2 0x18000090 /* DDR Extended Mode 2 Value */
#define DDR_EMR3 0x18000094 /* DDR Extended Mode 3 Value */
#define DDR_BURST 0x18000098 /* DDR Burst Control */
#define AHB_MASTER_TIMEOUT_MAX 0x1800009C /* AHB Master Timeout Control */
#define AHB_MASTER_TIMEOUT_CURNT 0x180000A0 /* AHB Timeout Current Count */
#define AHB_MASTER_TIMEOUT_SLAVE_ADDR 0x180000A4 /* Timeout Slave Address */

/* 6.2 UART Registers */
#define UART_DATA 0x18020000 /* UART Transmit and Rx FIFO */
#define UART_CS 0x18020004 /*UART  Configuration and Status */
#define UART_CLOCK 0x18020008 /* UART  Clock */
#define UART_INT 0x1804000C /* UART Interrupt */
#define UART_INT_EN 0x18040010 /* UART Interrupt Enable */

/* 6.3 USB Registers */
#define USB_PWRCTL 0x18030000 /* USB Power Control and Status */
#define USB_CONFIG 0x18030004 /* USB Configuration */

/* 6.4 GPIO Registers */
#define GPIO_OE 0x18040000 /* General Purpose I/O Output Enable */
#define GPIO_IN 0x18040004 /* General Purpose I/O Input Value */
#define GPIO_OUT 0x18040008 /* General Purpose I/O Output Value */
#define GPIO_SET 0x1804000C /* General Purpose I/O Bit Set */
#define GPIO_CLEAR 0x18040010 /* General Purpose I/O Per Bit Clear */
#define GPIO_INT 0x18040014 /* General Purpose I/O Interrupt Enable */
#define GPIO_INT_TYPE 0x18040018 /* General Purpose I/O Interrupt Type */
#define GPIO_INT_POLARITY 0x1804001C /* General Purpose I/O Interrupt Polarity */
#define GPIO_INT_PENDING 0x18040020 /* General Purpose I/O Interrupt Pending */
#define GPIO_INT_MASK 0x18040024 /* General Purpose I/O Interrupt Mask */
#define GPIO_FUNCTION_1 0x18040028 /* General Purpose I/O Function */
#define GPIO_IN_ETH_SWITCH_LED 0x1804002C /* General Purpose I/O Input Value */
#define GPIO_FUNCTION_2 0x18040030 /* Extended GPIO Function Control */

/* 6.5 PLL Control Registers */
#define CPU_PLL_CONFIG 0x18050000 /* CPU Phase Lock Loop Configuration */
#define CPU_PLL_CONFIG2 0x18050004 /* CPU Phase Lock Loop Configuration 2 */
#define CPU_CLOCK_CONTROL 0x18050008 /* CPU Clock Control */
#define PLL_DITHER_FRAC 0x18050010 /* CPU PLL Dither FRAC */
#define PLL_DITHER 0x18050014 /* CPU PLL Dither */
#define ETHSW_CLOCK_CONTROL 0x18050024 /* Ethernet Switch Clock Control */
#define ETH_XMII_CONTROL 0x1805002C /* Ethernet XMII Control */
#define SUSPEND 0x18050040 /* USB Suspend */
#define WLAN_CLOCK_CONTROL 0x18050044 /* WLAN Clock Control */

/* 6.6 Reset Control Registers */
#define RST_GENERAL_TIMER0 0x18060000 /* General Purpose Timer 0 */
#define RST_GENERAL_TIMER0_RELOAD 0x18060004 /* General Purpose Timer 0 Reload */
#define RST_WATCHDOG_TIMER_CONTROL 0x18060008 /* Watchdog Timer Control */
#define RST_WATCHDOG_TIMER 0x1806000C /* Watchdog Timer */
#define RST_MISC_INTERRUPT_STATUS 0x18060010 /* Miscellaneous Interrupt Status */
#define RST_MISC_INTERRUPT_MASK 0x18060014 /* Miscellaneous Interrupt Mask */
#define RST_GLOBAL_INTERRUPT_STATUS 0x18060018 /* Global Interrupt Status */
#define RST_RESET 0x1806001C /* Reset */
#define RST_REVISION_ID 0x18060090 /* Chip Revision ID */
#define RST_GENERAL_TIMER1 0x18060094 /* General Purpose Timer 1 */
#define RST_GENERAL_TIMER1_RELOAD 0x18060098 /* General Purpose Timer 1 Reload */
#define RST_GENERAL_TIMER2 0x1806009C /* General Purpose Timer 2 */
#define RST_GENERAL_TIMER2_RELOAD 0x180600A0 /* General Purpose Timer 2 Reload */
#define RST_GENERAL_TIMER3 0x180600A4 /* General Purpose Timer 3 */
#define RST_GENERAL_TIMER3_RELOAD 0x180600A8 /* General Purpose Timer 3 Reload */
#define RST_BOOT_STRAP 0x180600AC /* Boot Strap Status */
#define RST_USB_RST_CONTROL 0x180600B0 /* USB PHY Reset Control */

/* 6.7 GMAC Register */
#define ETH_CFG 0x18070000 /* Ethernet Configuration */

/* 6.8 SLIC Registers */
#define SLIC_SLOT 0x18090000 /* SLIC Slots Register */
#define SLICK_CLOCK_CONTROL 0x18090004 /* SLIC Clock Control */
#define SLIC_CTRL 0x18090008 /* SLIC Control */
#define SLIC_TX_SLOTS1 0x1809000C /* SLIC Tx Slots Register */
#define SLIC_TX_SLOTS2 0x18090010 /* SLIC Tx Slots 2 Register */
#define SLIC_RX_SLOTS1 0x18090014 /* SLIC Rx Slots Register */
#define SLIC_RX_SLOTS2 0x18090018 /* SLIC Rx Slots 2 Register */
#define SLIC_TIMING_CTRL 0x1809001C /* SLIC Timing Control Register */
#define SLIC_INTR 0x18090020 /* SLIC Interrupts Register */
#define SLIC_SWAP 0x18090024 /* SLIC Swap Registers */

/* 6.9 MBOX Registers */
#define MBOX_FIFO_STATUS 0x180A0008 /* Non-Destructive FIFO Status Query */
#define SLIC_MBOX_FIFO_STATUS 0x180A000C /* SLIC Non-Destructive FIFO Status Query */
#define MBOX_DMA_POLICY 0x180A0010 /* Mailbox DMA Engine Policy Control */
#define SLIC_MBOX_DMA_POLICY 0x180A0014 /* SLIC Mailbox DMA Engine Policy Control */
#define MBOX_DMA_RX_DESCRIPTOR_BASE 0x180A0018 /* Mailbox Rx DMA Descriptors Base Address */
#define MBOX_DMA_RX_CONTROL 0x180A001C /* Mailbox Rx DMA Control */
#define MBOX_DMA_TX_DESCRIPTOR_BASE 0x180A0020 /* Mailbox Tx DMA Descriptors Base Address */
#define MBOX_DMA_TX_CONTROL 0x180A0024 /* Mailbox Tx DMA Control */
#define SLIC_MBOX_DMA_RX_DESCRIPTOR_BASE 0x180A0028 /* SLIC Mailbox Rx DMA Descriptors Base Address */
#define SLIC_MBOX_DMA_RX_CONTROL 0x180A002C /* SLIC Mailbox Rx DMA Control */
#define SLIC_MBOX_DMA_TX_DESCRIPTOR_ 0x180A0030 /* BASE SLIC Mailbox Tx DMA Descriptors Base Address */
#define SLIC_MBOX_DMA_TX_CONTROL 0x180A0034 /* SLIC Mailbox Tx DMA Control */
#define MBOX_FRAME 0x180A0038 /* Mailbox FIFO Status */
#define SLIC_MBOX_FRAME 0x180A003C /* SLIC Mailbox FIFO Status */
#define FIFO_TIMEOUT 0x180A0040 /* FIFO Timeout Period */
#define MBOX_INT_STATUS 0x180A0044 /* MBOX Related Interrupt Status */
#define SLIC_MBOX_INT_STATUS 0x180A0048 /* SLIC MBOX Related Interrupt Status */
#define MBOX_INT_ENABLE 0x180A004C /* MBOX Related Interrupt Enables */
#define SLIC_MBOX_INT_ENABLE 0x180A0050 /* SLIC MBOX Related Interrupt Enables */
#define MBOX_FIFO_RESET 0x180A0058 /* MBOX Reset and Clear */
#define SLIC_MBOX_FIFO_RESET 0x180A005C /* SLIC MBOX Reset and Clear */

/* 6.10 I2S Registers */
#define STEREO0_CONFIG 0x180B0000 /* Configure Stereo Block */
#define STEREO0_VOLUME 0x180B0004 /* Set Stereo Volume */
#define STEREO0_TX_SAMPLE_CNT_LSB 0x180B000C /* Tx Sample Counter */
#define STEREO0_TX_SAMPLE_CNT_MSB 0x180B0010 /* Tx Sample Counter */
#define STEREO0_RX_SAMPLE_CNT_LSB 0x180B0014 /* Rx Sample Counter */
#define STEREO0_RX_SAMPLE_CNT_MSB 0x180B0018 /* Rx Sample Counter */
#define STEREO_CLK_DIV 0x180B001C /* Stereo Clock Divider */

/* 6.11 MDIO Slave Registers */
#define MDIO_HS_REG0 0x180B8000 /* MDIO Handshaking Register 0 */
#define MDIO_HS_REG1 0x180B8004 /* MDIO Handshaking Register 1 */
#define MDIO_HS_REG2 0x180B8008 /* MDIO Handshaking Register 2 */
#define MDIO_HS_REG3 0x180B800C /* MDIO Handshaking Register 3 */
#define MDIO_HS_REG4 0x180B8010 /* MDIO Handshaking Register 4 */
#define MDIO_HS_REG5 0x180B8014 /* MDIO Handshaking Register 5 */
#define MDIO_HS_REG6 0x180B8018 /* MDIO Handshaking Register 6 */
#define MDIO_HS_REG7 0x180B801C /* MDIO Handshaking Register 7 */
#define MDIO_ISR_REG 0x180B8020 /* MDIO ISR Register */

/* ... */


/* 6.20 Serial Flash Registers */
#define SPI_FUNCTION_SELECT 0x1F000000 /* SPI Function Select Register */
#define SPI_CONTROL 0x1F000004 /* SPI Control Register */
#define SPI_IO_CONTROL 0x1F000008 /* SPI IO Control */
#define SPI_READ_DATA 0x1F00000C /* SPI Read Data */
