/*
 * Copyright (C) 2013 Du Huanpeng <u74147@gmail.com>
 *
 * This file is part of barebox.
 * See file CREDITS for list of people who contributed to this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef __MACH_AR9331_DEBUG_LL__
#define __MACH_AR9331_DEBUG_LL__

/** @file
 *  This File contains declaration for early output support
 */
#include <io.h>
#include <mach/ar9331_regs.h>
#include <mach/ar9331_uart.h>

static __inline__ void PUTC_LL(char ch)
{
	while(((*(volatile unsigned *)0x18020000) & 0x200) == 0 );
	*(volatile unsigned *)0x18020000 = (ch & 0xFF) | 0x200;
}

#endif /* __MACH_AR9331_DEBUG_LL__ */

