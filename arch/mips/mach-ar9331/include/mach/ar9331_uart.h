#ifndef __AR9331_UART_H
#define __AR9331_UART_H

/* mask */
#define NEG1(type) (~((type)0))
#define NEG1UL NEG1(unsigned long)
#define BITS(h, l) (NEG1UL<<1<<(h) ^ NEG1UL<<(l))
/* #define MASK(type, h, l) ((NEG1(type)<<1<<(h)) ^ (NEG1<<(l))) */

/* 6.2.1 UART Transmit and Rx FIFO Interface (UART_DATA) */

#define UART_TX_CSR BITS(9, 9)
/* Read returns the status of the Tx FIFO. If set, the Tx FIFO can
 * accept more transmit data. Setting this bit will push
 * UART_TX_RX_DATA on the Tx FIFO. Clearing this bit has no
 * effect.
 */
#define UART_RX_CSR BITS(8, 8)
/*
 * Read returns the status of the Rx FIFO. If set, the receive data in
 * UART_TX_RX_DATA is valid. Setting this bit will pop the Rx
 * FIFO if there is valid data. Clearing this bit has no effect.
 */
#define UART_TX_RX_DATA BITS(7,0)
/* Read returns receive data from the Rx FIFO, but leaves the FIFO
 * unchanged. The receive data is valid only if UART_RX_CSR is also
 * set. Write pushes the transmit data on the Tx FIFO if
 * UART_TX_CSR is also set.
 */



#endif
